#!/usr/bin/php
<?php
const __ROOT__ = __DIR__ . DIRECTORY_SEPARATOR;

require_once(__ROOT__ . 'CheckBrackets.php');

do {
    $expression = readline('Enter expression for check: ');
} while (trim($expression) === '');

$checkBrackets = new CheckBrackets($expression);

if ($checkBrackets->run()) {
    echo 'Верно' . PHP_EOL;
} else {
    echo 'Не верно' . PHP_EOL;
}


