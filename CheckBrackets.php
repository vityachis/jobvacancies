<?php

/**
 * Class CheckBrackets
 */
class CheckBrackets
{
    /**
     * @var string
     */
    protected $expression;

    /**
     * @var string
     */
    protected $onlyBrackets;

    /**
     * CheckBrackets constructor.
     *
     * @param string $expression
     */
    public function __construct(string $expression)
    {
        $this->setExpression($expression);
    }

    /**
     * @param string $expression
     *
     * @return self
     */
    public function setExpression(string $expression): CheckBrackets
    {
        $this->expression   = trim($expression);
        $this->onlyBrackets = preg_replace('/[^(){}\[\]]/', '', $this->expression);

        return $this;
    }

    /**
     * @return bool
     */
    public function run(): bool
    {
        if (!$this->bracketsExist()) {
            return true;
        }

        if (!$this->validBracketsCount()) {
            return false;
        }

        if (!$this->checkBracketsInArithmeticExp()) {
            return false;
        }

        if ($this->checkPairedBrackets()) {
            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    protected function bracketsExist(): bool
    {
        return $this->onlyBrackets !== '';
    }

    /**
     * @return bool
     */
    protected function validBracketsCount(): bool
    {
        return strlen($this->onlyBrackets) % 2 === 0;
    }

    /**
     * @return bool
     */
    protected function checkBracketsInArithmeticExp(): bool
    {
        $noValidEqualityOfExpList = ['(=', '=)', '[=', '=]', '{=', '=}'];
        $bracketsAndEqual         = preg_replace('/[^(){}\[\]=]/', '', $this->expression);

        foreach ($noValidEqualityOfExpList as $val) {
            if (is_int(strpos($bracketsAndEqual, $val))) {
                return false;
            }
        }

        return true;
    }

    /**
     * @return bool
     */
    protected function checkPairedBrackets(): bool
    {
        $stringToCheck        = $this->expression;
        $searchPairedBrackets = ['()', '[]', '{}'];

        do {
            $oldStrLength  = strlen($stringToCheck);
            $stringToCheck = str_replace($searchPairedBrackets, '', $stringToCheck);
            $newStrLength  = strlen($stringToCheck);
        } while ($newStrLength > 0 && $newStrLength !== $oldStrLength);

        return $stringToCheck === '';
    }
}
