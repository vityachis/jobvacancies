SELECT tt1.id
FROM temp_table tt1
WHERE tt1.id IN (
    SELECT id
    FROM temp_table tt2
    GROUP BY tt2.id
    HAVING COUNT(tt2.id) > 1
)
GROUP BY tt1.id;
